/* 
Annual Pay
Daniyar Aubekerov
September 19, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{
	
	float payAmount = 2200.0; //Declaring payAmount var and assign its value to 2200
	int payPeriods=26; //Declaring payPeriods var and assign its value to 26
	float annualPay; //Declaring payAmount var

	annualPay=payPeriods*payAmount; //calculating annualPay var

	cout<<"Annual Pay is: $"<<annualPay<<endl; //displaying annual pay to the screen


	return 0; 
}
