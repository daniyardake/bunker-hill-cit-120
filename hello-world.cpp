/* 
Display Hello There Program
Daniyar Aubekerov
September 17, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{
	cout<<" Hello, there! "<<endl; 
    //Displaying "Hello, there" message on user's screen

	return 0; 
}
