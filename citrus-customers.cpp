/* 
Citrus Customers
Daniyar Aubekerov
September 30, 2018
*/ 

#include<iostream> //We are using input-output library
using namespace std;

int main() //main function
{
	const int customers = 16500; //Declaring customers variable of const int type
	double customers_drinking = customers * 0.15; //declaring and calculating value of customer_drinking variable of double type
	double customers_drinking_citrus = customers_drinking * 0.58; //declaring and calculating value of customer_drinking_citrus variable of double type


	cout << "The approximate number of customers in the survey who purchase one or more energy drinks per week is: "<< customers_drinking<<endl; //Displaying message on the user's screen with the result 1
	cout << "The approximate number of customers in the survey who prefer citrus-flavored energy drinks is: "<< customers_drinking_citrus<<endl; //Displaying message on the user's screen with the result 2
	

	return 0; //function doesn't return anything
}
