/* 
Car Gas Mileage
Daniyar Aubekerov
September 26, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{
	double galons, miles, per_galon; //Declaring variables of number of galons, number of miles(full tank) and per galon variable
	cout<<endl<<"Enter the number of gallons of gas the car can hold: "; //Displayng message asking for input files
	cin>>galons; //reading input file for galons
	
	cout<<endl<<"Enter he number of miles it can be driven on a full tank: "; //Displayng message asking for input files
	cin>>miles; //reading input file for miles

	per_galon=miles/galons; //calculating value of the number of miles that may be driven per gallon of gas
	cout<<endl<<"the number of miles that may be driven per gallon of gas: "<<per_galon<<endl; //Displaying the number of miles that may be driven sper gallon of gas
	return 0; 
}
