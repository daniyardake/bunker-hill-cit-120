/* 
Story
Daniyar Aubekerov
September 30, 2018
*/

#include<iostream> //We are using input/output library
using namespace std;

int main() //main function
{
	string name, city, college, profession, animal, name_pet; //declaring name, city, college, profession, animal, name_pet variables of string types
	int age; //declaring age variable of integer type


	cout<<"What is your name: "; //Displaying message on the user's screen asking information
	cin>>name; //reading name variable from keyboard


	cout<<"How old are you: "; //Displaying message on the user's screen asking information
	cin>> age; //reading age variable from keyboard

	cout<<"Where do you live: "; //Displaying message on the user's screen asking information
	cin>> city; //reading city variable from keyboard

	cout<<"Where do you study: "; //Displaying message on the user's screen asking information
	cin>> college; //reading college variable from keyboard

	cout<<"What is your profession: "; //Displaying message on the user's screen asking information
	cin>>profession; //reading profession variable from keyboard

	cout<<"Type of animal: "; //Displaying message on the user's screen asking information
	cin>>animal; //reading animal variable from keyboard

	cout<<"Pet's name: "; //Displaying message on the user's screen asking information
	cin>>name_pet; //reading name_pet variable from keyboard



	cout<<"There once was a person named "<<name<<" who lived in "<<city<<"."<<" At the age of "<< age<<", "<<name<<" went to college at "<<college<<". "; //Displaying message on the user's screen
	cout<<name<<" graduated and went to work as a "<<profession<<". Then, "<<name<<" adopted a(n) "<<animal<< " named " << name_pet<<". "<<" They both lived happily ever after!"<<endl; //Displaying message on the user's screen


	return 0; //function doesn't return anything
}



