/* 
Test Score
Daniyar Aubekerov
October 1, 2018
*/

#include<iostream> //We are using input/output library
using namespace std;

int main() //main function
{
	double n1, n2, n3; //delacring three variables of double type  

    cout << "Test one score: ";//Displaying message on user's screen
    cin >>n1 ; //reading value of n1

    cout<<endl<<"Test two score: ";//Displaying message on user's screen
    cin>>n2; //reading value of n2

    cout<<endl<<"Test three score: ";//Displaying message on user's screen
    cin>>n3; //reading value of n3


   double average = (n1+n2+n3)/3; //delacring and assigning value of average as average of n1, n2 and n3  
   const int HIGH_SCORE = 90; //declaring constant variable HIGH SCORE

	if(average > HIGH_SCORE) //checking if average more than HIGH SCORE
   		cout<<"You have scored more than High average"<<endl;//Displaying message on user's screen
   	else //else if it is less than or equal
   		cout<<"You have scored less than or equal to High average"<<endl;//Displaying message on user's screen


	return 0; //function doesn't return anything
}

