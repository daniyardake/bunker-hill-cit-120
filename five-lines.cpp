/* 
Display 5 lines Program
Daniyar Aubekerov
September 17, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{
	cout<<"Hello Teacher \n\n"; //Display Hello Teacher and make 1 line space
	cout<<"Hello Student \n\n\n"; //Display Hello Student and make 2 line space
	cout<<"How are you? \n\n\n\n"; //Display How are you? and make 3 line space
	cout<<"I am fine and you? \n\n\n\n\n"; //Display I am fine and you? and make 4 line space
	cout<<"I am good, thank you \n\n\n\n\n\n"; //Display I am good, thank you and make 5 line space

	return 0; 
}
