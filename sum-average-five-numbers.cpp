/* 
Sum and average of 5 numbers
Daniyar Aubekerov
September 19, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{
	
	const int a = 23, b = 43, c = 32, d = 22, e = 89;
    int sum; //decalring 5 variables and assigning their values
	float average; // decalring float (because of division by 5) varibale for average

	sum = a + b + c + d + e; //calculating the sum
	average=sum/5; //calculating the average
	cout<<"The sum is: "<<sum<<"\n the average is: "<<average<<endl; //printing the result on the screen

	return 0; 
}
