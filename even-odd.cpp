/* 
Even Odd
Daniyar Aubekerov
October 1, 2018
*/

#include<iostream> //We are using input/output library
using namespace std;

int main() //main function
{
	int n1; //Declaring variable n1 of integer type

    cout << "Your number: "; //Displaying message on user's screen
    cin >>n1 ; //reading the value of n1

   if (n1%2==0) //checking if this number is divisible by 2 then it is even
        cout<<n1<<" is even"<<endl; //Displaying message on user's screen
    else //else it is odd
        cout<<n1<<" is odd"<<endl; //Displaying message on user's screen


	return 0; //function doesn't return anything
}

