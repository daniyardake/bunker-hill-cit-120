/* 
Joe's Shares
Daniyar Aubekerov
September 30, 2018
*/ 

#include<iostream> //We are using input/output library
using namespace std;

int main() //main function
{
	const int shares = 1000; //declaring shares variable of const int type
	const double per_share_initial = 45.50; //declaring per_share_initital variable of const double type
	double stock = shares * per_share_initial; //declaring and calculating value of stock variable of double type
	double stockbrocker_initial = stock * 0.02; //declaring and calculating value of stockbrocker_initial variable of double type

	const double per_share_second = 56.90; //declaring per_share_second variable of const double type
	double recieved = shares * per_share_second; //declaring and calculating value of recieved variable of double type
	double stockbroker_second = recieved * 0.02; //declaring and calculating value of stockbroker_second variable of double type

	cout<<"Joe paid $"<<stock<<" for stock"<< endl; //Displaying message on the user's screen
	cout<<"Joe paid $"<<stockbrocker_initial<<" commission to his broker when he bought the stock"<< endl; //Displaying message on the user's screen
	cout<<"Joe sold his stock for $"<<recieved<< endl; //Displaying message on the user's screen
	cout<<"Joe paid $"<<stock<<" for stock"<< endl; //Displaying message on the user's screen
	cout<<"Joe paid $"<<stockbroker_second<<" commission to his broker when he sold the stock"<< endl; //Displaying message on the user's screen

	double total_profit = ( recieved - stockbroker_second ) - ( stock + stockbrocker_initial ) ; //declaring and calculating value of total_profit of double type

	cout<<"Joe's profit is $"<<total_profit<<endl; //Displaying message on the user's screen




	return 0; //function doesn't return anything
}