/* 
Cinema Profit
Daniyar Aubekerov
September 30, 2018
*/ 

#include<iostream> //input/output library
using namespace std;

int main() //main function
{
	string Name; //Declaring Name variable of String type
	cout<<"What is the name of the film: "; //Displaying message on the user's screen asking input
	cin>>Name; //read the value of Name variable

	int ticket_adult; //declaring ticket_adult variable of integer type
	cout<<endl<<"How many adult tickets were sold: "; //Displaying message on the user's screen asking input
	cin>> ticket_adult; //read the value of ticket_adult variable

	int ticket_child; //declaring ticket_child variable of integer type
	cout<<endl<<"How many child tickets were sold: "; //Displaying message on the user's screen asking input
	cin>> ticket_child; //read the value of ticket_child variable

	int total_profit = ticket_adult * 10 + ticket_child * 6; //declaring and calculating value of total_profit variable of integer type

	double office_profit = 0.2 * total_profit; //declaring and calculating value of office_profit variable of double type
	double distributor = total_profit - office_profit; //declaring and calculating value of distributor variable of double type

	cout<<endl<<"Movie Name: " <<Name<<endl; //Displaying message on the user's screen
	cout<<"Adult Tickets Sold: "<<ticket_adult<<endl; //Displaying message on the user's screen 
	cout<<"Child Tickets Sold: "<<ticket_child<<endl; //Displaying message on the user's screen
	cout<<"Gross Box Office Profit: $"<<total_profit<<endl; //Displaying message on the user's screen
	cout<<"Net Box Office Profit: $"<<office_profit<<endl; //Displaying message on the user's screen
	cout<<"Amount Paid to Distributor: $"<<distributor<<endl; //Displaying message on the user's screen



	return 0; //function doesn't return anything
}
