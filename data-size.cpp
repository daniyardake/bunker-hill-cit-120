/* 
Data size
Daniyar Aubekerov
September 19, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{

//Declaring 5 vars with their data types
	int age;
	char grade;
	float sum;
	double average;
	bool pass;

//printing their data sizes
	cout<<"age is integer variable uses "<<sizeof(age)<<" bytes"<<endl;
	cout<<"grade is char variable uses "<<sizeof(grade)<<" bytes"<<endl;
	cout<<"sum is float variable uses "<<sizeof(sum)<<" bytes"<<endl;
	cout<<"average is double variable uses "<<sizeof(average)<<" bytes"<<endl;
	cout<<"pass is bool variable uses "<<sizeof(pass)<<" bytes"<<endl;

	return 0; 
}
