/*
Meal Menu
Daniyar Aubekerov
Final Project CIT120
*/

#include <iostream>
#include <cmath>

using namespace std;

void ShowMenu();
void ReviewOrder();
void MakePayment();
void AdminMode();

double food_number[5][5];
double burger_DnBurger = 1.2;
double burger_DnChicken = 2.3;
double burger_Original = 3.14;
double burger_DoubleChicken = 4.1;

double wrap_Dnw = 2.2;
double wrap_Chricken = 3.3;
double wrap_GGG = 4.0;
double wrap_Super = 5.2;

double drink_Dan = 1.2;
double drink_Spryti = 1.6;
double drink_Cocona = 1.3;
double drink_Sodaaa = 2.2;
	
double dessert_Dookies = 0.95;
double dessert_Doreo = 1.3;
double dessert_Ice = 2.0;
double dessert_Vacanty = 15.2;


double total_sum;
int password = 1234;

int main()
{

	ShowMenu();
	
	return 0;
}

void ShowMenu()
{
	string choice_type;

	cout<<"\n\n\n*********************************"<<endl<<"This is MAIN MENU"<<endl<<"Choose one of the following please: "<<endl;
	cout<<"*********************************\n";
	cout<<"1: Burgers"<<endl;
	cout<<"2: Wraps"<<endl;
	cout<<"3: Drinks"<<endl;
	cout<<"4: Desserts"<<endl;
	cout<<"*********************************\n";
	cout<<"5: Review your order"<<endl;
	cout<<"6: Admin Mode"<<endl;
	cout<<"*********************************\n\n\n";

	cout<<endl<<"Your choice (1-6): ";
	cin>>choice_type;

	if (choice_type == "1")
	{
		system("clear");
		double choice_burger;
		cout<<"\n\n\n*********************************"<<endl;
		cout<<"Choose one of the following please: "<<endl;
		cout<<"*********************************"<<endl;

		cout<<"1: DnBurger $"<<burger_DnBurger<< endl;
		cout<<"2: DnChicken $"<<burger_DnChicken<<endl;
		cout<<"3: Original KZ Burger $"<<burger_Original<<endl;
		cout<<"4: Double chicken burher $"<<burger_DoubleChicken<<endl;
		cout<<"*********************************"<<endl;
		cout<<"5: Go back to main menu"<<endl;
		cout<<"*********************************"<<endl;
		cout<<endl<<"Your choice: ";
		cin>>choice_burger;

		for(int i=1; i<=4; i++)
		{
			int add=0;

			if(choice_burger == i)
			{
			
				cout<<endl<<"How many: ";
				cin>>add;
				food_number[1][i]+=add;
				ShowMenu();
			}

		}

		if(choice_burger ==5)
			ShowMenu(); 

		if ((choice_burger>5)||(choice_burger<1))
		{
			cout<<"\n\n\n!!!!!!ERROR!!!\n\n\n";
			ShowMenu();
		}
	}

	else if (choice_type == "2")
	{
		double choice_wrap;
		cout<<"\n\n\n*********************************"<<endl;
		cout<<"Choose one of the following please: "<<endl;
		cout<<"*********************************"<<endl;
		cout<<"1: DnwRAP $"<<wrap_Dnw<< endl;
		cout<<"2: ChrickenW $"<<wrap_Chricken<<endl;
		cout<<"3: GGG wrap $"<<wrap_GGG<<endl;
		cout<<"4: SUPER MEGA BIG WRAP $"<<wrap_Super<<endl;
		cout<<"*********************************"<<endl;
		cout<<"5: Go back to main menu"<<endl;
		cout<<"*********************************"<<endl;
		cout<<endl<<"Your choice: ";
		cin>>choice_wrap;

		for(int i=1; i<=4; i++)
		{
			int add=0;

			if(choice_wrap == i)
			{
			
				cout<<endl<<"How many: ";
				cin>>add;
				food_number[2][i]+=add;
				ShowMenu();
			}

		}

		if(choice_wrap ==5)
			ShowMenu(); 

		if ((choice_wrap>5)||(choice_wrap<1))
		{
			cout<<"\n\n\n!!!!!!ERROR!!!\n\n\n";
			ShowMenu();
		}
	}

	else if (choice_type == "3")
	{
		double choice_drink;
		cout<<"\n\n\n*********************************"<<endl;
		cout<<"Choose one of the following please: "<<endl;
		cout<<"*********************************"<<endl;
		cout<<"1: DanDrink $"<<drink_Dan<<endl;
		cout<<"2: Spryti $"<<drink_Spryti<< endl;
		cout<<"3: Cocona $"<<drink_Cocona<< endl;
		cout<<"4: Sodaaa $"<<drink_Sodaaa<< endl;
		cout<<"*********************************"<<endl;
		cout<<"5: Go back to main menu"<<endl;
		cout<<"*********************************"<<endl;
		cout<<endl<<"Your choice: ";
		cin>>choice_drink;

		for(int i=1; i<=4; i++)
		{
			int add=0;

			if(choice_drink == i)
			{
			
				cout<<endl<<"How many: ";
				cin>>add;
				food_number[3][i]+=add;
				ShowMenu();
			}

		}

		if(choice_drink ==5)
			ShowMenu(); 

		if ((choice_drink>5)||(choice_drink<1))
		{
			cout<<"\n\n\n!!!!!!ERROR!!!\n\n\n";
			ShowMenu();
		}
	}

	else if (choice_type == "4")
	{
		double choice_dessert;
		cout<<"\n\n\n*********************************"<<endl;
		cout<<"Choose one of the following please: "<<endl;
		cout<<"*********************************"<<endl;
		cout<<"1: Dookies $"<<dessert_Dookies<<endl;
		cout<<"2: Doreo $"<<dessert_Doreo<<endl;
		cout<<"3: Ice Cream KZ $"<<dessert_Ice<<endl;
		cout<<"4: Vacanty cream $"<<dessert_Vacanty<<endl;
		cout<<"*********************************"<<endl;
		cout<<"5: Go back to main menu"<<endl;
		cout<<"*********************************"<<endl;
		cout<<endl<<"Your choice: ";
		cin>>choice_dessert;

		for(int i=1; i<=4; i++)
		{
			int add=0;

			if(choice_dessert == i)
			{
			
				cout<<endl<<"How many: ";
				cin>>add;
				food_number[4][i]+=add;
				ShowMenu();
			}

		}

		if(choice_dessert ==5)
			ShowMenu(); 

		if ((choice_dessert>5)||(choice_dessert<1))
		{
			cout<<"\n\n\n!!!!!!ERROR!!!\n\n\n";
			ShowMenu();
		}
	}

	else if (choice_type == "5")
		ReviewOrder();

	else if (choice_type == "6")
		AdminMode();

	else 
	{
		cout<<"\n\n\n!!!!!!ERROR!!!\n\n\n";
		ShowMenu();
	}
}

void ReviewOrder()
{
	cout<<"\nAt this moment you ordered: \n****************\n";

	
	if(food_number[1][1]>0)
		cout<<food_number[1][1]<<" DnBurgers\n";

	if(food_number[1][2]>0)
		cout<<food_number[1][2]<<" DnChickens\n";

	if(food_number[1][3]>0)
		cout<<food_number[1][3]<<" Original KZ Burgers\n";

	if(food_number[1][4]>0)
		cout<<food_number[1][4]<<" Double chicken burhers\n";

	if(food_number[2][1]>0)
		cout<<food_number[2][1]<<" DnwRAPs\n";

	if(food_number[2][2]>0)
		cout<<food_number[2][2]<<" ChinkenWs\n";

	if(food_number[2][3]>0)
		cout<<food_number[2][3]<<" GGG wraps\n";

	if(food_number[2][4]>0)
		cout<<food_number[2][4]<<" SUPER MEGA BIG WRAPs\n";

	if(food_number[3][1]>0)
		cout<<food_number[3][1]<<" DanDrinks\n";

	if(food_number[3][2]>0)
		cout<<food_number[3][2]<<" Sprytis\n";

	if(food_number[3][3]>0)
		cout<<food_number[3][3]<<" Coconas\n";

	if(food_number[3][4]>0)
		cout<<food_number[3][4]<<" Sodaaas\n";

	if(food_number[4][1]>0)
		cout<<food_number[4][1]<<" Dookies\n";

	if(food_number[4][2]>0)
		cout<<food_number[4][2]<<" Doreos\n";

	if(food_number[4][3]>0)
		cout<<food_number[4][3]<<" Ice Cream KZs\n";

	if(food_number[4][4]>0)
		cout<<food_number[4][4]<<" Vacanty Creams\n";

	cout<<"\n*************\n";
	total_sum = food_number[1][1]*1.2+food_number[1][2]*2.3+food_number[1][3]*3.14+food_number[1][4]*4.1+food_number[2][1]*2.2+food_number[2][2]*3.3+food_number[2][3]*4+food_number[2][4]*5.2+food_number[3][1]*1.2+food_number[3][2]*1.6+food_number[3][3]*1.3+food_number[3][4]*2.2+food_number[4][1]*0.95+food_number[4][2]*1.3+food_number[4][3]*2+food_number[4][4]*15.2;

	cout<<endl<<"Your total sum is: $"<<total_sum<<"\nDo you want to pay now?\n\n\n **********\n1: Pay now \n2: Go back to menu\n";

	string choice_order;
	cin>>choice_order;

	if (choice_order=="1")
		MakePayment();

	else if (choice_order=="2")
		ShowMenu();

	else 
		{
			cout<<"\n\nError!\n\n";
			ReviewOrder();
		}
}

void MakePayment()
{
	cout<<"Choose your payment method: \n1:Cash\n2:Debit/credit card\n3:Voucher\n4:Go back\n";
	string choice_payment;
	cin>>choice_payment;

	if(choice_payment=="1")
	{
		double banknote[1000];
		double total_paid=0;
		int i=1;
		cout<<"Insert banknotes or coins: \n";
		while (total_paid<total_sum)
		{
			cout<<"\n$"<<total_sum - total_paid<< " To pay now\n";
			cout<<"Banknote or coin number "<<i<<": ";
			cin>>banknote[i];
			total_paid+=banknote[i];
			i++;
		}
		cout<<"\nThank you!\n Your change: $"<<total_paid - total_sum<<endl;

	}

	else if(choice_payment=="2")
	{
		cout<<"\n\n\nSecured Danny(R) payment sYstem \n";
		cout<<"Please write your card number here:";
		long long card_number;
		cin>>card_number;

		if ( (card_number<10000000000000000) && (card_number>1000000000000000) )
			cout<<"\nThank you!\n\n";
		else 
		{
			cout<<"\nSorry it is an ERROR. Try again.\n";
			MakePayment();
		}
	}

	else if(choice_payment=="3")
	{
		cout<<"\n\n\nSecured Danny(R) payment sYstem \n";
		cout<<"Please write your COUPON number here:";
		long long coupon_number;
		cin>>coupon_number;

		if(coupon_number % 2018 == 0)
			cout<<"\nThank you!\n\n";

		else 
		{
			cout<<"\nSorry it is an ERROR. Try again.\n";
			MakePayment();
		}

	}

	else if(choice_payment=="4")
	{
		ReviewOrder();
	}

	else 
	{
		cout<<"\n\nError!\n\n";
		MakePayment();
	}
}

void AdminMode()
{
	

	long long int tryPass;

	cout<<"Hello admin!\nType your password ot type 0000 to return to main menu:";
	cin>>tryPass;

	if (tryPass == password)
	{

		cout<<"Welcome! What do you want to do today? \n1:Change password\n2:Change the price of food\n3:Go back to main menu\n\n\n\n\n\n";
		int choice_admin;
		cin>>choice_admin;

		if(choice_admin ==1)
		{
			cout<<"Type new password: ";
			long long int newPass;
			cin>>newPass;
			password=newPass;
			cout<<"\nSuccess!\n";
			ShowMenu();

		}

		else if (choice_admin == 2)
		{
		
			cout<<"Here is the list of all foods and prices: \n\n"<<endl;



			cout<<"1: DnBurger $"<<burger_DnBurger<< endl;
			cout<<"2: DnChicken $"<<burger_DnChicken<<endl;
			cout<<"3: Original KZ Burger $"<<burger_Original<<endl;
			cout<<"4: Double chicken burher $"<<burger_DoubleChicken<<endl;

			cout<<"\n\n\n5: DnwRAP $"<<wrap_Dnw<< endl;
			cout<<"6: ChrickenW $"<<wrap_Chricken<<endl;
			cout<<"7: GGG wrap $"<<wrap_GGG<<endl;
			cout<<"8: SUPER MEGA BIG WRAP $"<<wrap_Super<<endl;

			cout<<"\n\n\n9: DanDrink $"<<drink_Dan<<endl;
			cout<<"10: Spryti $"<<drink_Spryti<< endl;
			cout<<"11: Cocona $"<<drink_Cocona<< endl;
			cout<<"12: Sodaaa $"<<drink_Sodaaa<< endl;

			cout<<"\n\n\n13: Dookies $"<<dessert_Dookies<<endl;
			cout<<"14: Doreo $"<<dessert_Doreo<<endl;
			cout<<"15: Ice Cream KZ $"<<dessert_Ice<<endl;
			cout<<"16: Vacanty cream $"<<dessert_Vacanty<<endl<<endl<<endl;

			cout<<"0: Go Back To Main Menu"<<endl<<endl<<endl;

			cout<<"\nWhich price do you want to change (type number 1-16): ";

			int choice_change;
			cin>>choice_change;

			if (choice_change == 1)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>burger_DnBurger;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 2)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>burger_DnChicken;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 3)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>burger_Original;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 4)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>burger_DoubleChicken;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 5)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>wrap_Dnw;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 6)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>wrap_Chricken;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 7)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>wrap_GGG;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 8)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>wrap_Super;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 9)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>drink_Dan;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 10)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>drink_Spryti;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 11)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>drink_Cocona;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 12)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>drink_Sodaaa;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 13)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>dessert_Dookies;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 14)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>dessert_Doreo;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 15)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>dessert_Ice;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 16)
			{
				cout<<"\n\n\n Okay. What is new unit price? ";
				cin>>dessert_Vacanty;
				cout<<"\n\n Done! Thank you!";
				AdminMode();

			}

			if (choice_change == 0)
			{
				ShowMenu();
			}

		}	

		else if (choice_admin == 3)
		{
				ShowMenu();
		}
	

	}

	else if (tryPass==0000)
	{
		ShowMenu();
	}


	else 
	{
		cout<<"Sorry, your password is not right, try again...\n";
		AdminMode();

	}
}