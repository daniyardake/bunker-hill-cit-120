/* 
Restaraunt Bill
Daniyar Aubekerov
September 19, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{
	
	float meal_charge = 88.67; //declaring varible for meal price
	float tax = meal_charge*0.0675; //declaring and calculating tax rate
	float tip = (meal_charge+tax)*0.2; // declaring and calculating tips
	float total = meal_charge + tip + tax; // declaring and calculating total amnt

	cout<<"Meal costs: $"<<meal_charge<<endl<<"Taxes is not included: $"<<tax<<endl<<"Tips: $"<<tip<<endl<<"Total ammount: $"<<total<<endl;

	return 0; 
}
