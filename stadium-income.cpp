/* 
Stadium Income
Daniyar Aubekerov
September 26, 2018
*/ 

#include<iostream>
#include<iomanip>
using namespace std;

int main() //main function
{
	double A,B,C, income; //Declaring variables for number of tickets of each class sold and variable for income
	cout<<endl<<"How many tickets for class A  were sold: "; //Displayng message asking for number of class A tickets sold
	cin>>A; //reading input file for number of class A tickets sold
	cout<<endl<<"How many tickets for class B  were sold: "; //Displayng message asking for number of class B tickets sold
	cin>>B; //reading input file for number of class B tickets sold
	cout<<endl<<"How many tickets for class C  were sold: "; //Displayng message asking for number of class C tickets sold
	cin>>C; //reading input file for number of class C tickets sold
	float D=1.00;

	income=A*15.0+B*12.0+C*9.0; //calculating income
	cout<<endl<<"Stadium's Income: $"<<income<<".0"<<endl; //Displaying stadium's income

	return 0; 
}
