/* 
Sum of two given variables Program
Daniyar Aubekerov
September 17, 2018
*/ 

#include<iostream>
using namespace std;

int main() //main function
{
	
	const int a=10, b=3; //declare two variables and assign its values
	int sum=a+b; //declare sum variable and make it the sum of a and b
	cout<<"The sum of a and b is: "<<sum; //display a message and result of summing a and b



	return 0; 
}
