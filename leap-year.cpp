/* 
Leap Year
Daniyar Aubekerov
October 1, 2018
*/

#include<iostream> //We are using input/output library
using namespace std;

int main() //main function
{
	int year; //Declaring year variable with integer type

    cout << "Enter a year: "; //Displaying message on user's screen
    cin >> year; //reading year var

    if (year % 4 == 0) //Checking if year is divisible by 4, then it has a chance to be a leap
    {
        if (year % 100 == 0)//Checking if year is moreover divisible by 100, then it has a chance to be a leap
        {
            if (year % 400 == 0) //Checking if year is divisible by 400, then it is leap
                cout << year << " is a leap year."; //Displaying message on user's screen
            else //else it is not
                cout << year << " is not a leap year."; //Displaying message on user's screen
        }
        else //else it is not
            cout << year << " is a leap year."; //Displaying message on user's screen
    }
    else //else it is not
        cout << year << " is not a leap year."<<endl; //Displaying message on user's screen


	return 0; //function doesn't return anything
}



